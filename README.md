# Netapp - a toolkit library for distributed software

[![Build Status](https://drone.deuxfleurs.fr/api/badges/lx/netapp/status.svg)](https://drone.deuxfleurs.fr/lx/netapp)

Netapp is a Rust library that takes care of a few common tasks in distributed software:

- establishing secure connections
- managing connection lifetime, reconnecting on failure
- checking peer's state
- peer discovery
- query/response message passing model for communications
- multiplexing transfers over a connection
- overlay networks: full mesh, and byzantine-tolerant random peer sampling using [Bᴀsᴀʟᴛ](https://arxiv.org/abs/2102.04063).

See examples folder to learn how to use netapp.
