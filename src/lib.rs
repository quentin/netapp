//! Netapp is a Rust library that takes care of a few common tasks in distributed software:
//!
//! - establishing secure connections
//! - managing connection lifetime, reconnecting on failure
//! - checking peer's state
//! - peer discovery
//! - query/response message passing model for communications
//! - multiplexing transfers over a connection
//! - overlay networks: full mesh, and soon other methods
//!
//! Of particular interest, read the documentation for the `netapp::NetApp` type,
//! the `message::Message` trait, and `proto::RequestPriority` to learn more
//! about message priorization.
//! Also check out the examples to learn how to use this crate.

pub mod error;
pub mod util;

pub mod endpoint;
pub mod proto;

mod client;
mod proto2;
mod server;

pub mod netapp;
pub mod peering;

pub use crate::netapp::*;
pub use util::{NetworkKey, NodeID, NodeKey};

#[cfg(test)]
mod test;
